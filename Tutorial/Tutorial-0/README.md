# Tutorial 0: Basics of Running FHI-aims

### Two Examples: Spin-unpolarized and Spin-polarized Molecules

In the following, we demonstrate two simple structure optimization runs using FHI-aims: 

- for the water molecule, H$_2$O 
- for the oxygen molecule, O$_2$ 

The contrast of these two examples illustrates a key lesson: The spin state of a molecule or solid matters. 

H$_2$O is a closed-shell molecule. 

However, O$_2$ is actually spin-polarized in its gas-phase ground state. It has two unpaired electrons, which assume the same spin state to form a paramagnetic molecule in nature.

### Fast Track

We strongly recommend to read through this tutorial in its entirety - even if you already have some experience with FHI-aims, you may find a few useful items here. However, if you are absolutely sure you already know what you are doing and just want to test things quickly, skip ahead to the final points of this tutorial:

  - Spin-unpolarized Simulation: Relaxation of the H$_2$O Molecule
  - Spin-polarized Simulation: Relaxation of the O$_2$ Molecule  

For maximum efficiency, we recommend [GIMS](https://gims.ms1p.org) ("Simple Calculation" workflow app)to build the input files.

### Background

This is example illustrates that it is critical to consider the spin state of a given molecule or solid as an input parameter to an electronic structure code, ***on equal footing with the initial geometry guess***. The spin state is ***NOT*** an automatic output of a simulation - already because spin-polarized systems usually have more than one spin state that could be stabilized, even if only one such spin state is the ground state and all other possible spin states are not.

Specifically, the H$_2$O molecule can safely be assumed to be non-spinpolarized in almost any practical scenario. Therefore, the simulation can be run in a nonmagnetic variant, without considering spin. For a molecule or solid that is truly non-magnetic, running a spin-polarized simulation would cost significantly more time and might even lead to an unstable self-consistent field cycle and/or a wrong spin state as the outcome.

In contrast, two possible spin states can be stabilized for O$_2$: non-magnetic, i.e., spin-paired (higher energy and chemically much more reactive) or paramagnetic, i.e., two unpaired spins (lower energy and much less chemically reactive). In order to simulate the correct, physically desired state (usually, the ground state), it is essential to provide the relevant initial information to FHI-aims and, at the end of the simulation, verify that the desired state was in fact obtained. 

Consider that you are likely sitting in front of a keyboard as you read this, immersed in an atmosphere with a significant partial pressure of the second-most chemically reactive element in the world: Oxygen. If this oxygen were non-magnetic, none of us would be sitting here in our present form. The fact that O$_2$ is paramagnetic (spin-polarized) in its ground state is a critical determinant of its properties. No simulation will get the Earth-atmospheric, gas-phase properties of O$_2$ right if spin is not considered. 

### Main Requirements

Here we briefly explain the main requirements for running a simulation with FHI-aims. To run a simulation with FHI-aims, we need the following:

- **FHI-aims executable:**
  
   - The executable can be obtained by compiling the code following the instruction in the FHI-aims manual. A complete overview is also given at the FHI-aims wiki, particularly this page: <https://aims-git.rz-berlin.mpg.de/aims/FHIaims/-/wikis/CMake%20Tutorial>
   - The FHI-aims binary usually has a name such as `aims.[label].x` where `[label]` is the label of the build, including a version stamp and type of build. For example, the latest binary built by VB at the time of writing is called `aims.210513.scalapack.mpi.x`.

- **Input files:**

  - FHI-aims requires only two input files, called `control.in` and `geometry.in` (please use **EXACTLY** these names), which must be located in the directory from which FHI-aims is called.
  - The format of `geometry.in` is a simple and general specification of the nature (chemical element) and type of atomic positions found in  a molecule or cluster (non-periodic) or in a three-dimensional periodic representation of a crystal, surface, or nanostructure. A general description of the `geometry.in` file format can be found here: Blum, Volker (2020): FHI-aims File Format Description: geometry.in. figshare. Online resource. <https://doi.org/10.6084/m9.figshare.12413477.v1> Further, FHI-aims specific keywords are described in the [FHI-aims manual](https://aims-git.rz-berlin.mpg.de/aims/FHIaims/-/jobs/artifacts/master/raw/doc/manual/FHI-aims.pdf?job=build.manual).
  - **`control.in`** contains all other parameters that control the simulation, e.g., the type of the density functional used, tolerances of the simulation, requested tasks and output, etc. 
  - Importantly, `control.in` must also contain a description of the numerical parameters ("species defaults", see below) associated with every chemical element (species) found in `geometry.in`. For density-functional theory, these "species defaults" are pre-tabulated at different levels of precision and numerical cost. While they usually do not need to be modified by a user, they do need to be appended in full to the `control.in` file. This practice enables users to modify any numerical parameters used in the simulation, should that be needed - simply by keeping the relevant parameters open and accessible.
  
- **Output files:**

- By default, the output of FHI-aims is not written to a file but rather to the standard output stream of Unix. In order to capture it in a file, the output stream must be redirected to a file (see below). 
- Unless other output is specifically requested in `control.in`, no other output files are written.
- The standard output stream of FHI-aims is deliberately kept human-readable and it deliberately contains much information, including diagnostic information that can be immensely valuable in order to understand what might have happened in a particular, usually complex simulation. 

- **Choice of the numerical settings ("species defaults"), including the basis set:**

  - FHI-aims uses numerically tabulated, atom-centered orbital (NAO) basis sets to represent the quantum mechanical orbitals and wave functions. These basis functions are specific to each chemical element (``species'') used in the simulation.
  -  FHI-aims also employs a variety of other numerical parameters that are specific to each atom and/or chemical element: Angular-momentum expansion order of the electrostatic (Hartree) potential, atom-centered real-space integration grids, etc.
  - Predefined files that contain these numerical definitions for each chemical element are located in **`species_defaults`** directory of the FHI-aims code distribution. At least four different levels of precision are provided for each chemical element: "light", "intermediate", "tight", and "really_tight". A more accurate choice of defaults means a higher computational cost of the simulation.
  
**In the present set of tutorials, "light" settings will be used throughout, for the sake of computational efficiency.** 

We note that "light" settings are intended to give qualitatively correct results. However, for "final" publication-quality data, we recommend to obtain all final results at least at the level of "intermediate" species defaults. A full set of "intermediate" species defaults can be found in the directory `species_defaults/defaults_2020/intermediate`. 

"tight" species defaults for "final" results are recommended particularly if the anticipated results rely on subtle energy differences (this is often the case for problems involving conformations of weakly bonded, flexible molecular species, for example). However, "tight" settings can be unnecessarily expensive particularly for hybrid density functionals and/or for materials with a high density of atoms per volume (e.g., many oxides or nitrides). 
  
### ASCII Editors vs. GIMS

`geometry.in` and `control.in` input files, as well as `aims.out` output files can, in principle, be prepared with any text editor capable of ASCII editing. At the Unix command line, the "emacs" editor or the "vim" editor are good tools. Both editors can be fully operated using the keyboard. 

Remember that, on a remote supercomputer, access is usually restricted to keyboard access only, i.e., the fact that both "emacs" or "vim" support keyboard-only operation is an advantage, not a historic abberation. The very instruments that we use for science (i.e., supercomputers) will, for security reasons, not support any other form of access.

Nevertheless, being able to graphically prepare input files before transfering them to a supercomputer and being able to graphically analyze the output after transfering it back to a local computer can be immensely helpful. This is what [GIMS](https://gims.ms1p.org) does. 

GIMS deliberately does not support (nearly) all modes of operation of FHI-aims, but it can simplify a rather important subset. To name a few:

- Build a structure from scratch or import a `geometry.in` file (including support for converting from other formats, such as .cif), modify/manipulate the structure and export to a new  file `geometry.in`.
- Prepare `control.in` files for different types of FHI-aims calculations.
- Import output of the simulation, e.g. `aims.out` file, and visualize key aspects of a simulation, including if and how s.c.f. convergence was achieved, how a structure optimization proceeded, etc.
- Visualize band structures and densities of states of periodic materials, including visualization and manipulation of the Brillouin zone.

![](./images/gims-homepage.png)
  
We will use [GIMS](https://gims.ms1p.org) frequently in the tutorials.

### Important Practical Recommendation

One calculation - one directory.

On the computer intended to run FHI-aims, you will need to manage files using a command line interface and in different folders ("directories") intended to organize your data.

We strongly recommend to create a new directory (with its own control.in and geometry.in files) for every new FHI-aims calculation.

Rerunning and/or continuing FHI-aims calculations in the same directory that was used before will overwrite output and input files. In any simulation (including non-trivial failed attempts), keeping the data can be essential to later understanding and/or reconstructing what happened in a particular calculation.

### How to Execute FHI-aims

After preparing the input files in the simulation folder, e.g. `my-simulation/control.in` and `my-simulation/geometry.in`, a parallel simulation with `N` processes can be launched by typing

    mpirun -n N aims.x > aims.out 2>&1 
    or
    mpirun -n N aims.x | tee aims.out

Here we write the output of the simulation to `aims.out` file but you can choose any other name that you prefer. 

The binary name `aims.x` should be replaced with whatever is the name of the FHI-aims binary file compiled by you (including the corresponding path, i.e., the location of the directory in which that file is located). For example, on this writer's laptop, the current binary name is `/Users/blum/codes/fhi-aims/bin/aims.210513.scalapack.mpi.x`.

The `mpirun` command facilitates the parallel execution of the code and can have different names that depend on the particular computer system and/or queueing system used. The actual name and usage are usually documented by the computing center in question.

### Finding a Local Minimum Structure of a Molecule, Solid, or Nanostructure

Finding atomic coordinates and (optionally) lattice parameters that correspond to a local minimum of the potential-energy surface at a given level of theory is usually the first step of an electronic structure based simulation. This step ensures that the input structure used to derive observables later is mathematically well defined and that the structure does not contain any accidentally misconfigured chemical configurations or bonds that are far from thermodynamic equilibrium conditions.

It is important to remember that, for a given set of atoms, there is usually more than one such local minimum. Finding the global minimum and/or a physically meaningful structure **cannot** usually be accomplished by just entering a random initial structure and hoping for the best. 

Setting up an initial structure verifying that it is physically plausible is a critical task that can be the key step in a scientific project. Often, this step amounts to formulating the overarching scientific question correctly in the first place. 

Likewise, **visualizing** an input structure before using it for expensive follow-up simulations is a critical task that should never be skipped. Check for physically implausible bond lengths, bond angles, misplaced atoms, etc. 

There are many good visualization applications. In this tutorial, we use [GIMS](https://gims.ms1p.org). However, [Jmol](http://jmol.sourceforge.net/), [Vesta](https://jp-minerals.org/vesta/en/), [XCrysDen](http://www.xcrysden.org/) and others are all excellent and frequently used tools for visualization tasks related to FHI-aims. 

### Notes About Structure Relaxation in FHI-aims

The terms "local structure optimization" and "structure relaxation" are used interchangeably in the following. They mean the same thing.

- FHI-aims can calculate gradients of the total energy with respect to atomic positions ("forces") and lattice parameters ("stresses") for local-density, generalized-gradient and meta-generalized gradient density functionals, as well as hybrid density functionals. 
- A variant of the [Broyden–Fletcher–Goldfarb–Shanno](https://en.wikipedia.org/wiki/Broyden%E2%80%93Fletcher%E2%80%93Goldfarb%E2%80%93Shanno_algorithm) (`bfgs`) algorithm, enhanced by a [trust-region method](https://en.wikipedia.org/wiki/Trust_region) (`trm`), is employed in FHI-aims to find a local minimum of the potential energy surface. Such a minimum is considered to be found if the moduli of all forces should be smaller than a small, configurable threshold value, typically smaller than $5\cdot10^{-3}$ eV/Å. Within FHI-aims, the terms `bfgs` and `trm` are used to denote the same algorithm.
- It is highly worthwhile to understand at least the basics of the optimization algorithms mentioned above - follow the links. Importantly, this shows that the [`bfgs`](https://en.wikipedia.org/wiki/Broyden%E2%80%93Fletcher%E2%80%93Goldfarb%E2%80%93Shanno_algorithm) algorithm relies on a good approximation the Hessian matrix, that is, the second derivatives of the total energy with respect to all combinations of atomic and/or lattice coordinates for a given structure. A good initial guess for the Hessian matrix at the outset of a structure optimization can be immensely helpful to speed up the calculation. We use a variant of the initial guess by [Lindh and coworkers](https://doi.org/10.1016/0009-2614(95)00646-L) but if this not works, a simple diagonal initial guess for the Hessian can also be used. 
- Even more importantly, the `bfgs` algorithm updates and improves its own guess of the Hessian matrix as the structure optimization progresses. Structure optimization is a step-wise process. At each optimization step, FHI-aims writes the current atomic/lattice geometry into a file `geometry.in.next_step`. It also writes a file `hessian.aims`, which contains the current guess of the Hessian matrix determined by the `bfgs` algorithm.
- To start a new simulation with the relaxed structure, `geometry.in.next_step` should be used as the `geometry.in` file of the new simulation, and `hessian.aims` should be present in the new simulation folder. For example, one can copy `old-simulation/geometry.in.next_step` into `new-simulation/geometry.in`, prepare appropriate `control.in`, and then start the new simulation.

### Restarting a Structure Relaxation

The files `geometry.in.next_step` and `hessian.aims` are the two key pieces needed to restart a structure relaxation from the results of an earlier relaxation. Such a restart can be desirable for several reasons, e.g, in order to:

- Continue an unfinished relaxation that has stopped (for example, due to a queue wall time limit or because the earlier FHI-aims relaxation run ecountered a problem and stopped with an error message).
- Use the data created by a pre-relaxation run using "light" settings and follow up with a post-relaxation using "intermediate" or "tight" settings.
- Use the data generated by pre-relaxing with a cheaper density functional (e.g., PBE) as a starting point to initialize a post-relaxation with a more expensive density functional (e.g., HSE06).

The method to restart a structure relaxation from an earlier one is simple:

- Run the pre-relaxation, which generates intermediate files `geometry.in.next_step` and `hessian.aims`
- After the pre-relaxation is finished, create a new directory for the followup relaxation 
- Copy the `geometry.in.next_step` and `hessian.aims` files from the pre-relaxation directory to the followup-relaxation directory. 
- Change the name of the copy of `geometry.in.next_step` to `geometry.in`. 
- Create a new `control.in` file in the followup-relaxation directory
- Rerun FHI-aims in the follow-up directory.

### Critical Note Regarding Pre-Relaxation

We always recommend to begin a relaxation run with "light" settings and, in some cases, even with a cheaper density functional (assuming a reliable cheaper density functional can be found). The resulting, pre-optimized geometry (`geometry.in.next_step`) can be MUCH better than the initial guess and the resulting approximate Hessian matrix (`hessian.aims`) can be MUCH better than an initial guess.

In order to follow up with better computational settings ("intermediate" or "tight") and/or with a more sophisticated but more expensive density functional (e.g., HSE06 after PBE), simply copy over the preceding `geometry.in.next_step` file to `geometry.in` in a new directory, likewise copy over the corresponding `hessian.aims` file, create a new `control.in` file with the desired computational settings, and begin a fresh FHI-aims run.

In contrast, beginning a relaxation from a bad geometry guess while using highly converged numerical settings throughout the entire structure optimization can be very expensive and entirely unnecessary (most of the intermediate structures of the relaxation will likely never be used again). Thus, always:

- Pre-relax with appropriate, relatively cheap settings (usually, "light")
- Post-relax using tighter settings in a second run, using the information generated during the pre-relaxation as a starting point.

### Level of Theory: Choice of Density Functional and Choice of Scalar Relativity

In order to fully define the potential energy surface explored by FHI-aims, the choice of a density functional is needed. This choice is not always easy and does require some knowledge about the strengths and weaknesses of a given density functional for a different problem.

In the present tutorial, we use the PBE density functional, selected using the `xc` keyword in `control.in` (see below).

A second important choice in any DFT calculation is the approximation used to represent the kinetic energy. Specifically and for chemistry and materials science, we should all use the Dirac Equation, not the Schrödinger Equation. The relativistic Dirac Equation captures essentially all effects relevant to chemistry and materials science without any significant inaccuracies, whereas the non-relativistic Schrödinger form of the kinetic energy is acceptable only for the lightest chemical elements and becomes progressively wrong as elements grow heavier - even for valence electrons, not just for deep core states. 

Yet, the numerical form of the Schrödinger Equation is computationally much simpler and also less demanding than Dirac's Equation. In practice, the solution adopted by practically every production code is to replace the Schrödinger kinetic energy with a "scalar relativistic" kinetic energy that captures the most important aspects of relativity, while retaining the same numerical shape as the Schrödinger Equation.

In our experience, there is never a reason not to use scalar relativity - by sticking to a single form of the kinetic energy operator in all calculations, consistent total energy differences, eigenvalues and other observables can be ensured.

Conversely, mixing total energies from scalar- and non-relativistic treatments in energy differences could be a catastrophic idea since the total energies of the same system can be very different numbers for different relativistic treatment.

The particular form of scalar relativity employed in FHI-aims can and must be invoked in `control.in` through the keywords

`relativistic   atomic_zora scalar`

The particular expressions that define this level of scalar relativity can be found in Eqs. (55) and (56) of Computer Physics Communications 180, 2175-2196 (2009). <http://dx.doi.org/10.1016/j.cpc.2009.06.022>.

More importantly, this level of scalar relativity has proven to be extremely robust and accurate for valence electron related properties over time, consistently matching benchmark results of other high-precision codes in the community: see. e.g., <http://dx.doi.org/10.1126/science.aad3000> for bulk cohesive properties and <https://doi.org/10.1103/PhysRevMaterials.1.033803> for a broad range of energy band structures.

In short, we recommend to simply always use `relativistic   atomic_zora scalar` as outlined above.


### Spin-unpolarized vs. Spin-polarized Simulations

The ***spin-state*** of a material or molecule in FHI-aims is controlled by keyword ***`spin`*** in FHI-aims:

- For systems known to be spin-unpolarized, `spin none` is the correct keyword. This choice avoids unnecessary computational complexity and traps. We will use this for H$_2$O.

- For suspected spin polarized systems, on the other hand, the true ground-state can only be obtained with a spin-polarized simulation. `spin collinear` is the correct keyword. There is, however, an unavoidable complication for spin-polarized systems. Usually, more than one spin state can be stabilized in a simulation (and often even experimentally). Therefore, a spin-polarized simulation requires a **correct initialization** of the spin configuration of the material. 

- For spin-polarized simulations, a correct spin initialization must therefore also be provided as input to the simulation. This should be done in `geometry.in`, by adding specific keywords `initial_moment` after each atom that should carry an initial spin. For instance, `initial_moment 1.0` corresponds to one more spin-up electron than spin-down electron in the initial atomic density for that atom, used to initialize the self-consistent field cycle. An improper spin initialization may cause the simulation to describe a state which is not the true ground-state of the system.

- After a spin-polarized simulation is complete, the spin state may be a different one than the spin state provided in the initialization. This final spin-state must be carefully checked at the end of a simulation, in order to correctly understand the final result of a calculation. For many systems with stable spin ground-states, this task is straightforward. However, for systems with competing spin states, much care and attention can be required to get the spin state right.

- There are good reasons why we recommend against a blanket spin-polarized initialization of all atoms with a certain default initial moment. For any system of reasonable complexity, such an initialization will almost certainly correspond to a physically unreasonable initialization. For instance, a high-spin state (all spins oriented equally) will invariably be assumed, ruling out any antiferromagnetic solutions practically a priori.

- Additionally, a blanket spin-polarized initialization can cause severe or insurmountable problems with convergence of the self-consistent field cycle, if the assumed spin configuration is far from the actual ground state. This can cost lots of computer time spent on exploring entirely irrelevant parts of the electronic configurational space of a material or molecule. Thinking about the desired spin state a priori is a critical part of ensuring efficient simulations that rapidly lead to physically correct conclusions.

We therefore always recommend (in practice, we insist) on providing a thoughtful spin initialization in the `geometry.in` file of a spin-polarized simulation, using `initial_moment` for each atom that might carry a spin. There are usually multiple possible solutions and even severe technical pitfalls associated with incorrect spin guesses. An electronic structure code can, in practice, not be able to guess which one of potentially many possible physical scenarios a user is trying to be explore.
  
### Spin-unpolarized Simulation: Relaxation of the H$_2$O Molecule

This tutorial takes the H$_2$O molecule as an example for a "light" relaxation. The starting conformation assumes an unphysical bond angle of 90°. After structure optimization (here, carried out using the PBE exchange-correlation functional), a much more physical angle results.

The initial conformation `geometry.in` of the molecule and the initial `control.in` file can be built using [GIMS](https://gims.ms1p.org) and the "Simple Calculation" workflow app. Both files can equally easily be constructed using a text editor.

Using GIMS for this task does have the advantage that the "species defaults" for H and O will be added to `control.in` automatically. When using a text editor and the command line, the species defaults need to be appended to `control.in` manually using an appropriate command (see below). 

  - `control.in`:

        xc             pbe
        relativistic   atomic_zora scalar
        relax_geometry bfgs        5e-3 
        spin           none
  
Compared to the default output of GIMS, we here added the `spin none` keyword. This setting is, in fact, the default, i.e., FHI-aims will perform a spin-unpolarized calculation unless asked to do otherwise.

We are also using the `bfgs` (= `trm`) algorithm for geometry optimization, as described above. We will consider a local minimum of the total energy to be reached when the magnitude of all residual forces on the atomic nuclei is smaller than $5\cdot10^{-3}$ eV/Å.

In order to obtain the complete `control.in` file, the "light" species defaults for the H and O atoms will still need to be appended to  `control.in`. 

At the command line, the appropriate command is `cat [FHI-aims-directory]/species_defaults/light/01_H_default >> control.in` and `cat [FHI-aims-directory]/species_defaults/light/08_O_default >> control.in`, where `[FHI-aims-directory]` should be replaced with the proper path to your FHI-aims distribution on the computer to be used.

  - `geometry.in`

        atom    0.00000000    0.00000000    0.00000000    O
        atom    0.70700000   -0.70700000    0.00000000    H 
        atom   -0.70700000   -0.70700000    0.00000000    H 

After the FHI-aims calculation is complete (see above for the command to execute FHI-aims), the fully relaxed structure ("light") looks like this:

- `geometry.in.next_step` 
  
      # 
      # This is the geometry file that corresponds to the current relaxation step.
      # If you do not want this file to be written, set the "write_restart_geometry" flag to .false.
      #  aims_uuid : 9496E7B2-D8E2-4C5F-AA87-D4B66DF85A6B                                
      # 
      atom       0.00000000     -0.07320102      0.00000000 O
      atom       0.76731112     -0.67039949      0.00000000 H
      atom      -0.76731112     -0.67039949      0.00000000 H
      # 
      # What follows is the current estimated Hessian matrix constructed by the BFGS algorithm.
      # This is NOT the true Hessian matrix of the system.
      # If you do not want this information here, switch it off using the "hessian_to_restart_geometry" keyword.
      # 
      trust_radius             0.2000000030
      hessian_file

A file `hessian.aims` is also written. As outlined above, the `geometry.in.next_step` and `hessian.aims` files could be used to continue the relaxation with "intermediate" or "tight" species defaults and obtain fully converged results.

It is furthermore worthwhile to take a look at the actual FHI-aims output stream (written into a file `aims.out` in our example). 

This stream begins with some important information regarding how the calculation was executed, then includes complete copies of `control.in` and `geometry.in`, details all self-consistent field iterations, as well as relaxation steps. The end of `aims.out`` is a line

` Have a nice day.`

This line is used as FHI-aims' indicator that the calculation converged as expected. (Also, this line was among the first, if not the first line written in FHI-aims in 2004 and the sentiment is absolutely genuine.)

A bit before the end of the standard output `aims.out`, one can identify the final total energy delivered by the s.c.f. cycle, as well as the final geometry reached by the geometry optimization.

This information, and more, can also be obtained by opening the `Output Analyzer` App of GIMS and importing the respective `aims.out` file there. One advantage of GIMS is that it does provide a graphical analysis of the calculation, particularly of the self-consistent field cycle and of successive geometry steps until a converged geometry is found.


### Spin-polarized Simulation: Relaxation of the O$_2$ Molecule

The process for O$_2$ is very similar to the preceding example (H$_2$O), with the critical example that the ground state of O$_2$ is known to be spin-polarized. If we just followed the example of H$_2$O above, we would in fact obtain a perfectly well converged solution, but for the wrong spin state (zero spin-polarization) - an unphysical state of the molecule. Clearly, attention must be paid to how the calculation is initialized in order to arrive at the correct result.

The key difference is that we select `spin collinear` in `control.in` and that we specify an initial spin moment for the atoms in `geometry.in`.

  - `control.in`: 

        ################################
        # PARAMETRES OF THE SIMULATION #
        ################################
        xc             pbe
        relativistic   atomic_zora scalar
        relax_geometry bfgs        5e-3
        spin           collinear


  Also, we add a `light` basis set to `control.in`. This can be done by using command `cat species_defaults/light/08_O_default >> control.in`.


  - `geometry.in` (initial structure): 

        atom    0.0     0.0     0.0   O
            initial_moment   1.0
            initial_charge   0.0
        atom    0.0     0.0     1.9   O
            initial_moment   1.0
            initial_charge   0.0

In order to pick a good initial spin moment, we do indeed require some chemical knowledge. Alternatively, we could do a systematic search by testing all reasonable combinations of positive and negative initial moments (remember, spin configurations can be parallel or antiparallel in a simple scalar-relativistic picture). With the systematic search, we would find all possible spin-polarized solutions of the O$_2$ molecule. A look into the Chemistry textbook convinces us that the O$_2$ molecule in our atmosphere has two unpaired electrons, that is, one unpaired electron per O atom. This is why we choose `initial_moment   1.0`.

Here we also set an `initial_charge` of `0.0` for O atoms. We will see later that in case of an ionic material such as Fe$_2$O$_3$ clusters, choosing the right initial charge can drastically improve the structure relaxation. Alas, at the time of writing, FHI-aims supports initial charges for non-periodic systems only, whereas initial moments are supported for both non-periodic and periodic structures. It is the intention of the authors to provide support for non-zero initial charges also for periodic systen geometries soon.

After relaxation is finished, the final structure of the molecule looks like this:

- `geometry.in.next_step`  

      # 
      # This is the geometry file that corresponds to the current relaxation step.
      # If you do not want this file to be written, set the "write_restart_geometry" flag to .false.
      #  aims_uuid : E7CC723F-28B2-42B7-A3FF-552868BF4B42                                
      # 
      atom       0.00000000      0.00000000      0.33687123 O
      atom       0.00000000      0.00000000      1.56312877 O
      # 
      # What follows is the current estimated Hessian matrix constructed by the BFGS algorithm.
      # This is NOT the true Hessian matrix of the system.
      # If you do not want this information here, switch it off using the "hessian_to_restart_geometry" keyword.
      # 
      trust_radius             0.2000000030
      hessian_file

In the `aims.out` file resulting from this run, we can confirm that the FINAL total spin moment printed for the O$_2$ molecule is indeed 2:

    [...]
     Current spin moment of the entire structure :
     | N = N_up - N_down :    2.00
     | S                 :    1.00
     | J                 :    3.00
   
     Highest occupied state (VBM) at     -6.90564250 eV
     | Occupation number:      1.00000000
     | Spin channel:        1
   
     Lowest unoccupied state (CBM) at    -4.63949581 eV
     | Occupation number:      0.00000000
     | Spin channel:        2

     Overall HOMO-LUMO gap:      2.26614669 eV.
     | Chemical Potential                          :    -5.44135584 eV

     Self-consistency cycle converged.
     [...]

Of course, similar information can be found by importing `aims.out` to the `Output Analyzer` App of GIMS.

Note that the "chemical potential" mentioned here refers to the approximate Fermi level between occupied and unoccupied states. The O$_2$ molecule has a non-zero HOMO-LUMO gap. From a computational point of view, the electronic "chemical potential" is therefore somewhat arbitrary since all that is needed is to place it somewhere in the HOMO-LUMO gap. For systems with a HOMO-LUMO gap (also semiconductors), the printed electronic "chemical potential" is therefore not very meaningful and can fluctuate seemingly randomly within the gap, between different systems. For systems with an energy gap, the highest occupied state (HOMO or VBM) would be a more reproducible choice for the reference energy of electrons.

However, for our purposes, we are now set to go. We know about charge and spin initialization, how to find a local-minimum energy structure, a total energy for simple molecules such as H$_2$O and O$_2$. What is left is to do some more intricate science using the same approaches for more complex systems.
