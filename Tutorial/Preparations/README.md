# Preparations

## 1. First comes first

At the command line, create a folder for this tutorial and a handy environment variable for configurations:

```
mkdir COST-Tutorials
cd COST-Tutorials
export COSTHOME=$(pwd)
```

## 2. How to install FHI-aims

1. Make sure you are in the `COST-Tutorials` folder.
2. Clone FHI-aims via:
  ```
  git clone git@aims-git.rz-berlin.mpg.de:aims/FHIaims.git
  ```
2. Follow the instructions on this page: [https://aims-git.rz-berlin.mpg.de/aims/FHIaims/-/wikis/CMake%20Tutorial]()

## 3. How to install clims

1. Make sure you are in the `COST-Tutorials` folder.
2. Clone clims via:
  ```
  git clone git@gitlab.com:FHI-aims-club/utilities/clims.git
  ```
3. Follow the installation instructions here: [https://gitlab.com/FHI-aims-club/utilities/clims/-/blob/master/README.md]()
4. Configure clims:
  ```
  clims-configure --species-path $COSTHOME/FHIaims/species_defaults
  ```

## 4. How to find GIMS

You can find GIMS here: [https://gims.ms1p.org](https://gims.ms1p.org/static/index.html)

## 5. Solutions to the tutorial can be found here:

<https://gitlab.com/FHI-aims-club/tutorials/COST-FHIaims-tutorial>
