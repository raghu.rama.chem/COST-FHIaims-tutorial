#!/usr/bin/python

#############
# Functions #
#############
def print_reaction_info(chemical_reaction, reaction_energy):
    print("{:50s} : dE = {:.4f} eV".format(chemical_reaction, reaction_energy))

def calculate_reaction_energies(cluster_bare_start,
                                cluster_bare_end,
                                cluster_plus_OH2, 
                                cluster_plus_OH,
                                cluster_plus_O,
                                cluster_plus_OOH,
                                H2O,
                                O2,
                                H2):        
    reaction_a = cluster_plus_OH2 - cluster_bare_start - H2O
    reaction_b = cluster_plus_OH  + .5 * H2 - cluster_plus_OH2
    reaction_c = cluster_plus_O   + .5 * H2 - cluster_plus_OH
    reaction_d = cluster_plus_OOH + .5 * H2 - cluster_plus_O - H2O
    reaction_e = cluster_bare_end + O2 + .5 * H2 - cluster_plus_OOH
    cumulative = reaction_a + reaction_b + reaction_c + reaction_d + reaction_e 
    electrochemical_reaction_potential = cumulative / 4

    chemical_reaction = "(A) H2O  + (*) --> *OH2"
    print_reaction_info(chemical_reaction, reaction_a)
    chemical_reaction = "(B) *OH2       --> *OH  + H+ + e-"
    print_reaction_info(chemical_reaction, reaction_b)
    chemical_reaction = "(C) *OH        --> *O   + H+ + e-"
    print_reaction_info(chemical_reaction, reaction_c)
    chemical_reaction = "(D) H2O  + *O  --> *OOH + H+ + e-"
    print_reaction_info(chemical_reaction, reaction_d)
    chemical_reaction = "(E) *OOH       --> (*)  + O2 + H+ + e-"
    print_reaction_info(chemical_reaction, reaction_e)
    chemical_reaction = "Cumulative reaction energy"
    print_reaction_info(chemical_reaction, cumulative)
    chemical_reaction = "Predicted electrochemical reaction potential"
    print_reaction_info(chemical_reaction, electrochemical_reaction_potential)  
    print 

############################
# Molecules total energies #
############################
H2  = -0.317402412992194E+02
H2O = -0.208083225449889E+04
O2  = -0.409321055881287E+04

############################
# Water splitting reaction #
############################
water_splitting_energy = O2 + 2 * H2 - 2 * H2O
electrochemical_reaction_potential = water_splitting_energy / 4

print("========================================")
print("Water splitting reaction energy from DFT")
print("========================================")
chemical_reaction = "2H2O --> O2 + 4H+ + 4e-"
print_reaction_info(chemical_reaction, water_splitting_energy)
chemical_reaction = "Predicted electrochemical reaction potential"
print_reaction_info(chemical_reaction, electrochemical_reaction_potential)
print

#############################
# Approach-1 total energies #
#############################
cluster_bare_start = -0.121085860435450E+07
cluster_plus_OH2   = -0.121294047733182E+07
cluster_plus_OH    = -0.121292371024209E+07
cluster_plus_O     = -0.121290651546379E+07
cluster_plus_OOH   = -0.121496960314986E+07
cluster_bare_end   = -0.121085864342153E+07

# Reactions
print("===============================")
print("Reaction energies of Approach 1")
print("===============================")
calculate_reaction_energies(cluster_bare_start,
                            cluster_bare_end,
                            cluster_plus_OH2, 
                            cluster_plus_OH,
                            cluster_plus_O,
                            cluster_plus_OOH,
                            H2O,
                            O2,
                            H2)

#############################
# Approach-2 total energies #
#############################
cluster_bare_start = -0.121085860435450E+07
cluster_plus_OH2   = -0.121294047733182E+07
cluster_plus_OH    = -0.121292371058536E+07
cluster_plus_O     = -0.121290651555157E+07
cluster_plus_OOH   = -0.121496946014808E+07
cluster_bare_end   = -0.121085864429400E+07

# Reactions
print("===============================")
print("Reaction energies of Approach 2")
print("===============================")
calculate_reaction_energies(cluster_bare_start,
                            cluster_bare_end,
                            cluster_plus_OH2, 
                            cluster_plus_OH,
                            cluster_plus_O,
                            cluster_plus_OOH,
                            H2O,
                            O2,
                            H2)
