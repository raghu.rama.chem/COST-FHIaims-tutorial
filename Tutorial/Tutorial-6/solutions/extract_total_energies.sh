#!/bin/bash

# molecules
DATA_DIR=molecules
OUT_FILE=total-energy.molecules
> $OUT_FILE
for aimsout in $DATA_DIR/*/aims.out; do
    system=$(echo $aimsout | cut -d/ -f2) 
    energy=$(grep uncorrected $aimsout | tail -1 | tr -s ' ' | cut -d' ' -f7)
    echo $system $energy >> $OUT_FILE
done

# approach 1
DATA_DIR=cluster/approach-1
OUT_FILE=total-energy.cluster-approach-1
> $OUT_FILE
for aimsout in $DATA_DIR/*/aims.out; do
    system=$(echo $aimsout | cut -d/ -f3) 
    energy=$(grep uncorrected $aimsout | tail -1 | tr -s ' ' | cut -d' ' -f7)
    echo $system $energy >> $OUT_FILE
done

# approach 2
DATA_DIR=cluster/approach-2
OUT_FILE=total-energy.cluster-approach-2
> $OUT_FILE
for aimsout in $DATA_DIR/*/aims.out; do
    system=$(echo $aimsout | cut -d/ -f3) 
    energy=$(grep uncorrected $aimsout | tail -1 | tr -s ' ' | cut -d' ' -f7)
    echo $system $energy >> $OUT_FILE
done