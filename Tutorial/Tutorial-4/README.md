# Tutorial 4: Building a stoichiometric cluster

The remaining tasks of Tutorials 4, 5, and 6 consist of geometry set-up tasks and total energy calculations, similar in spirit to Tutorial 0, except for larger and more complex structures.

As mentioned elsewhere, one critical step in defining our scientific question (effectiveness of a particular set of catalytic reaction steps at Fe$_2$O$_3$ nanoparticles) involves defining one or more hypotheses for the structure that such nanoparticles would assume.

We here pursue the hypothesis that such a cluster structure can be found by creating a reasonably-sized, maximally compact, stoichiometric cut-out from a bulk phase (hematite Fe$_2$O$_3$). This is a crude hypothesis an certainly not the only one, or the best motivated one. However, it will provide us with cluster models with well defined facets and edges, suitable for further testing of chemical reaction steps later.

We call the definition of these clusters "Wigner-Seitz" clusters, borrowed from the concept of a compact [Wigner-Seitz cell](https://en.wikipedia.org/wiki/Wigner%E2%80%93Seitz_cell) in a periodic crystal lattice. The DFT-PBE relaxed structure and spin state of the primive unit cell of hematite Fe$_2$O$_3$ will be our starting point.

We will also need to ensure a good charge and spin initialization of the electron density for our calculations. 

As laid out in detail in Tutorial 0, this step is an inherent step of setting up the input of an electronic structure calculation. Without considering at least the target spin state, a calculation may not converge at all or it may converge to an entirely unwanted, unphysical self-consistent solution of the spin state that is not the ground state or the experimentally desired state.

The physical principle for our chosen charge and spin initialization is simple, based on chemical expectations:

- Assume the formal charge state of an ionic solid: `initial_charge 3.0` for Fe and `initial_charge -2.0` for O. For a non-periodic system, the current version of FHI-aims allows such a charged initialization. This initialization can greatly help improve the initial charge density, moving it closer to the expected final charge distribution of an ionic solid.

- For the Fe(III) ions, assume a high-spin state - 5 d electrons, spin up: `initial_moment 5` or `initial_moment -5`, with up and down spins initially distributed according to the bulk spin ground state of hematite Fe$_2$O$_3$.

- For the O anions, assume zero initial moments.

Note that these choices only define the initial charge density - the actual charge and spin densities will sort themselves out during the s.c.f. cycle of an electronic structure calculation.

To create these initial geometry, charge, and spin states in `geometry.in`, we use a script (see below). The script takes the initial moments and charges specified in the primitive structure and constructs a larger supercell with identically initialized atoms. After the construction of this supercell, all atoms are folded back into the Wigner-Seitz cell and finally the lattice vectors are removed, to leave behind a non-periodic cut-out of the bulk structure. Et Voila: We have our cluster.

1. Initialize the atoms with proper spin moments and charges in the `geometry.in` file representing the PBE-relaxed primitive unit cell.

2. Execute the following script. To reproduce the solution `geometry-ws-cluster.in` we used the following clims command:

        clims-wigner-seitz-cluster --translate-basis 1 0 0 2 2 2
      
    and the relaxed structure (`geometry.in.next_step`) from the `Tutorial-2/Part-1/uudd_relaxation_light` folder. The clims command shifts the initial basis atoms by 1 A in the Cartesian x direction and creates a 2x2x2 supercell. Obviously, you could vary these parameters to obtain different compact initial cluster representations cut out of the hematite bulk lattice.

3. Spend some time playing with the translation of the basis atoms. Ideally, attempyt to find a cluster with only a few singly bonded outermost atoms. Atoms that are completely removed from their stable bonding environment will not be represented well in DFT. They will also typically not exist in low-energy, chemically reasonable atomic arrangements of a material. But worst of all, such unhappy atoms will often significantly impede the convergence of the s.c.f. cycle of DFT. The wave function associated with such atoms is often not well represented by a single Slater determinant (or by two single determinants in the case of a spin-polarized calculation), creating a significant difficulty for practical density functional approximations.

The cluster we have built here contains only 80 atoms. We here restrict ourselves to a small cluster size since this is already a large system for a tutorial, given that spin and charge density convergence of hematite clusters already pose significant computational challenges of their own.

However, we expect that 80-atom clusters will generally be too small to maintain a bulk hematite-like structure that might be found in an experiment. For publishable simulations of clusters with a stable hematite-like structure, larger structures could be created using the script infrastructure above.
